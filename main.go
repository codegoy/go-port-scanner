package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"sync"
	"time"
)

var commonPorts = map[uint16]string{
	1: "TCPMUX",
	5: "RJE",
	7: "ECHO",
	9: "DISCARD",
	11: "SYSTAT",
	13: "DAYTIME",
	17: "QOTD",
	18: "MSP",
	19: "CHARGEN",
	20: "FTP-DATA",
	21: "FTP",
	22: "SSH",
	23: "TELNET",
	25: "SMTP",
	37: "TIME",
	39: "RLP",
	42: "NAMESERVER",
	43: "NICNAME",
	49: "TACACS",
	50: "RE-MAIL-CK",
	53: "DOMAIN",
	63: "WHOIS++",
	67: "BOOTPS",
	68: "BOOTPC",
	69: "TFTP",
	70: "GOPHER",
	71: "NETRJS-1",
	72: "NETRJS-2",
	73: "NETRJS-3,NETRJS-4",
	79: "FINGER",
	80: "HTTP",
	88: "KERBEROS",
	95: "SUPDUP",
	101: "HOSTNAME",
	102: "ISO-TSAP",
	105: "CSNET-NS",
	107: "RTELNET",
	109: "POP2",
	110: "POP3",
	111: "SUNRPC",
	113: "AUTH",
	115: "SFTP",
	117: "UUCP-PATH",
	119: "NNTP",
	123: "NTP",
	137: "NETBIOS-NS",
	138: "NETBIOS-DGM",
	139: "NETBIOS-SSN",
	143: "IMAP",
	161: "SNMP",
	162: "SNMPTRAP",
	163: "CMIP-MAN",
	164: "CMIP-AGENT",
	174: "MAILQ",
	177: "XDMCP",
	178: "NEXTSTEP",
	179: "BGP",
	191: "PROSPERO",
	194: "IRC",
	199: "SMUX",
	201: "AT-RTMP",
	202: "AT-NBP",
	204: "AT-ECHO",
	206: "AT-ZIS",
	209: "QMTP",
	210: "Z39.50",
	213: "IPX",
	220: "IMAP3",
	245: "LINK",
	347: "FATSERV",
	363: "RSVP_TUNNEL",
	369: "RPC2PORTMAP",
	370: "CODAAUTH2",
	372: "ULISTPROC",
	389: "LDAP",
	427: "SVRLOC",
	434: "MOBILEIP-AGENT",
	435: "MOBILIP-MN",
	443: "HTTPS",
	444: "SNPP",
	445: "MICROSOFT-DS",
	464: "KPASSWD",
	468: "PHOTURIS",
	487: "SAFT",
	488: "GSS-HTTP",
	496: "PIM-RP-DISC",
	500: "ISAKMP",
	535: "IIOP",
	538: "GDOMAP",
	546: "DHCPV6-CLIENT",
	547: "DHCPV6-SERVER",
	554: "RTSP",
	563: "NNTPS",
	565: "WHOAMI",
	587: "SUBMISSION",
	610: "NPMP-LOCAL",
	611: "NPMP-GUI",
	612: "HMMP-IND",
	631: "IPP",
	636: "LDAPS",
	674: "ACAP",
	694: "HA-CLUSTER",
	749: "KERBEROS-ADM",
	750: "KERBEROS-IV",
	765: "WEBSTER",
	767: "PHONEBOOK",
	873: "RSYNC",
	992: "TELNETS",
	993: "IMAPS",
	994: "IRCS",
	995: "POP3S",
}

type Scanner struct {
	TIMEOUT time.Duration
	ADDRESS string
	PORT    uint16
}

func (s *Scanner) Connect() (err error) {
	_, err = net.DialTimeout("tcp", fmt.Sprintf("%s:%d", s.ADDRESS, s.PORT), s.TIMEOUT*time.Second)
	return
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf(`⚡️
Usage: %v [args]... <urls>...
Example: %v -t 10 10.0.0.11 10.0.0.1 10.0.0.2 10.0.0.3 forkedpath.dev 10.0.0.12 10.0.0.27
Flags:
		-t timeout
⚡️
`, os.Args[0], os.Args[0])
		return
	}
	var userTimeout time.Duration
	var argCount int
	for x := 1; x < len(os.Args); x++ {
		switch os.Args[x] {
		case "-t":
			argCount = argCount + 2
			i, _ := strconv.Atoi(os.Args[x+1])
			userTimeout = time.Duration(i)
		}
	}
	addresses := os.Args[1+argCount:]
	var wg sync.WaitGroup
	var x uint16
	portRange := []uint16{1, 65535}
	if userTimeout == time.Duration(0) {
		userTimeout = time.Duration(10)
	}
	for _, address := range addresses {
		//fmt.Println("scanning address::", address)
		for x = portRange[0]; x < portRange[1]; x++ {
			port := x
			wg.Add(1)
			go func() {
				s := Scanner{
					ADDRESS: address,
					PORT:    port,
					TIMEOUT: userTimeout,
				}
				if s.Connect() == nil {
					if portInfo, ok := commonPorts[s.PORT]; ok {
						fmt.Printf("OPEN PORT->%10s:%-7d| %s\n", s.ADDRESS, s.PORT, portInfo)
					} else {
						fmt.Printf("OPEN PORT->%10s:%-7d|\n", s.ADDRESS, s.PORT)
					}
				}
				wg.Done()
			}()
		}
	}
	wg.Wait()
}
